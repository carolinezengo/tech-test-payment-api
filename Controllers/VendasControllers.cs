using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers 
{

    [ApiController]
    [Route("api/[controller]")]
 public class VendasControllers : ControllerBase
 {
        private readonly VendaContext _context;

    public VendasControllers(VendaContext context)
    {
        _context = context;
    }

    [HttpPost("api-docs")]
    public IActionResult EfetuarVenda(Vendas venda)
    {
    _context.Add(venda);
    _context.SaveChanges();
    return Ok(venda);
    }

  
    [HttpPost("api-vendedor")]
    public IActionResult CriarVendedor(Vendedor vendedor)
    {
    _context.Add(vendedor);
    _context.SaveChanges();
    return Ok(vendedor);
    }
   
     [HttpGet("api-docs/{id}")]
    public IActionResult ObterVendaPorID(int id)
    {
      var vendas = _context.Vendas.Find(id);

      if(vendas == null)
      return NotFound();
      return Ok(vendas);
    }

   [HttpPut("api-docs/{id}")]
    public IActionResult AtualizarStatusPagamento(int id, Vendas vendas)
    {
      var vendaBanco = _context.Vendas.Find(id);

      if(vendaBanco == null)
      return NotFound();
      if(vendaBanco.TipoDePagamento == "Aguardando Pagamento")
      {
      vendaBanco.TipoDePagamento = "Aprovado";
      _context.Vendas.Update(vendaBanco);
      _context.SaveChanges();

      }
      else if (vendaBanco.TipoDePagamento == "Aprovado")
      {
      vendaBanco.TipoDePagamento = "Transportadora";
      _context.Vendas.Update(vendaBanco);
      _context.SaveChanges();

      }
      else if (vendaBanco.TipoDePagamento == "Transportadora")
      {
      vendaBanco.TipoDePagamento = "Entregue";
      _context.Vendas.Update(vendaBanco);
      _context.SaveChanges();

      }

      


    
           return Ok(vendaBanco);
      
    }
       
 }
}