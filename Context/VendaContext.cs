using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Entities;


namespace tech_test_payment_api.Context
{
    public class VendaContext : DbContext
    {
       public VendaContext(DbContextOptions<VendaContext> option) : base (option)
       {
            
       }
       public DbSet<Vendedor> Vendedores{get; set;}
        public DbSet<Vendas> Vendas{get; set;}
    }
}