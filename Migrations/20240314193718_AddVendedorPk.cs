﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace tech_test_payment_api.Migrations
{
    /// <inheritdoc />
    public partial class AddVendedorPk : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VendedoresId",
                table: "Vendas",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Vendas_VendedoresId",
                table: "Vendas",
                column: "VendedoresId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vendas_Vendedores_VendedoresId",
                table: "Vendas",
                column: "VendedoresId",
                principalTable: "Vendedores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vendas_Vendedores_VendedoresId",
                table: "Vendas");

            migrationBuilder.DropIndex(
                name: "IX_Vendas_VendedoresId",
                table: "Vendas");

            migrationBuilder.DropColumn(
                name: "VendedoresId",
                table: "Vendas");
        }
    }
}
