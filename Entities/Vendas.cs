using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Vendas
    {
        public int Id { get; set; }
        [ForeignKey("Vendedor")]
        public int IdVendedor { get; set; }
        public Vendedor Vendedores {get; set;}
        public string Item { get; set; }
        public string TipoDePagamento { get; set; }
    }
}